package Task::ADAMJS;
# ABSTRACT: Libraries/Application I use regularly

use strict;
use 5.008_005;
our $VERSION = '1.1';

1;
__END__

=head1 SYNOPSIS

  # cpanm https://github.com/battlemidget/Task-ADAMJS.git
  # task-adamjs-install

=head1 DESCRIPTION

Task::ADAMJS is collection of perl applications and libraries I find useful
and frequently depend on. Check the B<cpanfile> for list of dependencies.

=cut
